from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# You can generate a Token from the "Tokens Tab" in the UI
token = "M3BsOEgTIVc17T8bFv7LyhSv9AHtdPxYqJeVPbuMJHgIauIOgoA0CXVgSgQ_RsAhwQk4s_cCELYiAgUMizx_xA=="
org = "n3ll1x Co"
bucket = "ip"

client = InfluxDBClient(url="http://192.168.65.5:8086", token=token)
point = Point("mem").tag("host", "host1").field("used_percent", 23.43234543).time(datetime.utcnow(), WritePrecision.NS)

write_api = client.write_api(write_options=SYNCHRONOUS)
write_api.write(bucket, org, point)
#query = f'from(bucket: \\"{bucket}\\") |> range(start: -1h)'
#tables = client.query_api().query(query, org=org)
