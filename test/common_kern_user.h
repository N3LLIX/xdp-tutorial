/* This common_kern_user.h is used by kernel side BPF-progs and
 * userspace programs, for sharing common struct's and DEFINEs.
 */
#ifndef __COMMON_KERN_USER_H
#define __COMMON_KERN_USER_H

/* Header cursor to keep track of current parsing position */
struct hdr_cursor {
	void *pos;
};

struct metrics_sip{
	__u32 counter;
};

/* This is the data record stored in the map */
struct datarec {
	__u64 rx_packets;
	__u64 rx_bytes;
	__u64 rx_icmp;
	__u64 rx_tcp;
	__u64 rx_udp;
	__u64 rx_other;
};


#ifndef XDP_ACTION_MAX
#define XDP_ACTION_MAX (XDP_REDIRECT + 1)
#endif

#endif /* __COMMON_KERN_USER_H */
