#!/usr/bin/python
#
# net_monitor.py Aggregates incoming network traffic
# outputs source ip, destination ip, the number of their network traffic, and current time
# how to use : net_monitor.py <net_interface> 
# 
# Copyright (c) 2020 YoungEun Choe

from bcc import BPF
import time
from ast import literal_eval
import sys
import json
import ctypes


# def help():
#     print("execute: {0} <net_interface>".format(sys.argv[0]))
#     print("e.g.: {0} eno1\n".format(sys.argv[0]))
#     exit(1)

# if len(sys.argv) != 2:
#     help()
# elif len(sys.argv) == 2:
#     INTERFACE = sys.argv[1]

bpf_text = """

#include <uapi/linux/ptrace.h>
#include <net/sock.h>
#include <bcc/proto.h>
#include <linux/bpf.h>

#define IP_TCP 6
#define IP_UDP 17
#define IP_ICMP 1
#define ETH_HLEN 14


struct key_map {
	__u32 dst_address;
	__u32 src_address;
};

BPF_PERF_OUTPUT(skb_events);
BPF_HASH(packet_cnt, struct key_map, long, 256); 

int packet_monitor(struct __sk_buff *skb) {
    u8 *cursor = 0;
    u32 saddr, daddr;
    long* count = 0;
    long one = 1;
 //   u64 pass_value = 0;
    struct key_map key;
    
    struct ethernet_t *ethernet = cursor_advance(cursor, sizeof(*ethernet));

    struct ip_t *ip = cursor_advance(cursor, sizeof(*ip));
    if (ip->nextp != IP_TCP) 
    {
        if (ip -> nextp != IP_UDP) 
        {
            if (ip -> nextp != IP_ICMP) 
                return 0; 
        }
    }
    
    saddr = ip -> src;
    daddr = ip -> dst;

    key.src_address = saddr;
    key.dst_address = daddr;

/*
    pass_value = saddr;
    pass_value = pass_value << 32;
    pass_value = pass_value + daddr;
*/
    count = packet_cnt.lookup(&key); 
    if (count)  // check if this map exists
        *count += 1;
    else        // if the map for the key doesn't exist, create one
        {
            packet_cnt.update(&key, &one);
        }
    return -1;
}

"""

from ctypes import *
import ctypes as ct
import sys
import socket
import os
import struct
from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

OUTPUT_INTERVAL = 1


def ip2int(addr):
    return struct.unpack("!I", socket.inet_aton(addr))[0]


def int2ip(addr):
    return socket.inet_ntoa(struct.pack("!I", addr))

    
def save_into_influxdb(client,item, bucket,org):
    point = Point("address_count").tag("node",os.environ['NODE_NAME']).tag("namespace",os.environ['NAMESPACE_NAME']).tag("pod", str(socket.gethostname())).tag("source",item["source"]).tag("destination",item["destination"]).field("counter", item["counter"]).time(datetime.utcnow(), WritePrecision.NS)

    write_api = client.write_api(write_options=SYNCHRONOUS)
    write_api.write(bucket, org, point)


def retrieve_interface():
    interfaces = socket.if_nameindex()
    for interface in interfaces:
        iface = interface[1]
        if("e" in iface):
            INTERFACE = str(iface)
            print(str(INTERFACE))




bpf = BPF(text=bpf_text)

function_skb_matching = bpf.load_func("packet_monitor", BPF.SOCKET_FILTER)

INTERFACE = retrieve_interface()

BPF.attach_raw_socket(function_skb_matching, INTERFACE)

    # retrieeve packet_cnt map
packet_cnt = bpf.get_table('packet_cnt')    # retrieeve packet_cnt map
# You can generate a Token from the "Tokens Tab" in the UI
token = "M3BsOEgTIVc17T8bFv7LyhSv9AHtdPxYqJeVPbuMJHgIauIOgoA0CXVgSgQ_RsAhwQk4s_cCELYiAgUMizx_xA=="
organization = "n3ll1x Co"
bucketName = "ip"

client = InfluxDBClient(url="http://192.168.65.5:8086", token=token)


try:
    while True :
        time.sleep(OUTPUT_INTERVAL)
        packet_cnt_output = packet_cnt.items()
        output_len = len(packet_cnt_output)
        print('Passing len '+str(output_len))
        for k,v in packet_cnt_output:
            #print("Src : "+str(k.src_address))
       #     print("Address-Counter: "+str(k)+" "+str(v) )
       #     if (len(str(k))) != 30:
       #         print("Len cn out: "+str(len(str(k))))
       #         continue
       #     temp = int(str(k)[8:-2]) # initial output omitted from the kernel space program
       #     temp = int(str(bin(temp))[2:]) # raw file
       #     src = int(str(k)[:32],2) # part1 
      #      dst = int(str(k)[32:],2)
       #     pkt_num = str(v)

            monitor_result = {"source":  "", "destination": "", "counter": 0}
            monitor_result_json = json.dumps(monitor_result)

            monitor_result["source"] =  str(int2ip((int)(k.src_address)))
            monitor_result["destination"] =  str(int2ip((int)(k.dst_address)))
            monitor_result["counter"] = int(v.value)

            # "destination":  str(int2ip((int)(k.dst_address))), "counter": str(int(v))}
            # monitor_result = 'source address : ' + str(int2ip((int)(k.src_address))) + ' ' + 'destination address : ' + \
            # str(int2ip((int)(k.dst_address))) + ' ' + str(int(v)) + ' ' + 'time : ' + str(time.localtime()[0])+\
            # ';'+str(time.localtime()[1]).zfill(2)+';'+str(time.localtime()[2]).zfill(2)+';'+\
            # str(time.localtime()[3]).zfill(2)+';'+str(time.localtime()[4]).zfill(2)+';'+\
            # str(time.localtime()[5]).zfill(2)
            monitor_result_json = json.dumps(monitor_result)
            print(monitor_result_json)
            save_into_influxdb(client,monitor_result,bucketName,organization)

            # time.time() outputs time elapsed since 00:00 hours, 1st, Jan., 1970.
        packet_cnt.clear() # delete map entires after printing output. confiremd it deletes values and keys too 
        
except KeyboardInterrupt:
    sys.stdout.close()
    pass
