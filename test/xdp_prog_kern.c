
/* SPDX-License-Identifier: GPL-2.0 */
#include <stddef.h>
#include <linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/icmp.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>
/* Defines xdp_stats_map from packet04 */
#include "../common/xdp_stats_kern_user.h"
#include "../common/xdp_stats_kern.h"

/* Packet parsing helpers.
 *
 * Each helper parses a packet header, including doing bounds checking, and
 * returns the type of its contents if successful, and -1 otherwise.
 *
 * For Ethernet and IP headers, the content type is the type of the payload
 * (h_proto for Ethernet, nexthdr for IPv6), for ICMP it is the ICMP type field.
 * All return values are in host byte order.
 */
static __always_inline int parse_ethhdr(struct hdr_cursor *nh,
					void *data_end,
					struct ethhdr **ethhdr)
{
	struct ethhdr *eth = nh->pos;
	int hdrsize = sizeof(*eth);

	/* Byte-count bounds check; check if current pointer + size of header
	 * is after data_end.
	 */
	if (nh->pos + hdrsize > data_end)
		return -1;

	nh->pos += hdrsize;
	*ethhdr = eth;

	return eth->h_proto; /* network-byte-order */
}

/* Assignment 2: Implement and use this */
static __always_inline int parse_iphdr(struct hdr_cursor *nh,
					void *data_end,
					struct iphdr **iphdr)
{
	struct iphdr *ip = nh->pos;
	int hdrsize = sizeof(*ip);

	/* Byte-count bounds check; check if current pointer + size of header
	 * is after data_end.
	 */
	if (nh->pos + hdrsize > data_end)
		return -1;

	nh->pos += hdrsize;
	*iphdr = ip;

	return ip->protocol; /* network-byte-order */
}

/* Assignment 3: Implement and use this */
/*static __always_inline int parse_icmp6hdr(struct hdr_cursor *nh,
					  void *data_end,
					  struct icmp6hdr **icmp6hdr)
{
}*/

SEC("xdp_packet_parser")
int  xdp_parser_func(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	struct ethhdr *eth;
	struct iphdr *ip;
	__u32 saddr = 34;
	//__u32 *res;

	/* Default action XDP_PASS, imply everything we couldn't parse, or that
	 * we don't want to deal with, we just pass up the stack and let the
	 * kernel deal with it.
	 */
	__u32 action = XDP_PASS; /* Default action */

        /* These keep track of the next header type and iterator pointer */
	struct hdr_cursor nh;
	int nh_type;

	/* Start next header cursor position at data start */
	nh.pos = data;

	/* Packet parsing in steps: Get each header one at a time, aborting if
	 * parsing fails. Each helper function does sanity checking (is the
	 * header type in the packet correct?), and bounds checking.
	 */
	nh_type = parse_ethhdr(&nh, data_end, &eth);
	if (nh_type == -1)
		goto drop;

	/* Assignment additions go below here */
	nh_type = parse_iphdr(&nh, data_end, &ip);

	if (nh_type == -1)
		goto drop;

	saddr = ip->saddr;

	__u32 key_size = 0;
	__u32* value_size = 0;
	__u32 *index_map = bpf_map_lookup_elem(&ip_index, &saddr);
	if(!index_map)
		goto drop;

	if(*index_map == 0)
	{
		value_size = bpf_map_lookup_elem(&ip_index, &key_size);
		if(!value_size)
			goto drop;
		*value_size++; 
		*index_map = *value_size;
	}

	__u32 *ipaddress = bpf_map_lookup_elem(&index_ip, &value_size);
	if(!ipaddress)
		goto drop;
	*ipaddress = saddr;

/*
	struct metrics_sip *ptr_srcip = bpf_map_lookup_elem(&xdp_sip_map, index_map);
	if(!ptr_srcip)
		goto drop;

	if(*ptr_srcip.counter == 0){
		ptr_srcip->counter++;
		ptr_srcip->address = *saddr;
	}

*/
	// struct metrics_sip *rec = bpf_map_lookup_elem(&xdp_sip_map, ptr_srcip);
	// if (!rec)
	// 	goto drop;
	// rec->counter++;
	//  //rec->src_address = ip->saddr;

	goto out;


drop:
	action = XDP_DROP;
	return xdp_stats_record_action(ctx, action, 0); 
out:
	return xdp_stats_record_action(ctx, action, nh_type); /* read via xdp_stats */
}

char _license[] SEC("license") = "GPL";
