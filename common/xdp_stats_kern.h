/* SPDX-License-Identifier: GPL-2.0 */

/* Used *ONLY* by BPF-prog running kernel side. */
#ifndef __XDP_STATS_KERN_H
#define __XDP_STATS_KERN_H

/* Data record type 'struct datarec' is defined in common/xdp_stats_kern_user.h,
 * programs using this header must first include that file.
 */
#ifndef __XDP_STATS_KERN_USER_H
#warning "You forgot to #include <../common/xdp_stats_kern_user.h>"
#include <../common/xdp_stats_kern_user.h>
#endif

/* Keeps mapping between key xdp_sip_map and src ip */
struct bpf_map_def SEC("maps") ip_index = {
	.type        = BPF_MAP_TYPE_PERCPU_ARRAY,
	.key_size    = sizeof(__u32),
	.value_size  = sizeof(__u32),
	.max_entries = XDP_ACTION_MAX,
};

/* Keeps mapping between key xdp_sip_map and src ip */
struct bpf_map_def SEC("maps") index_ip = {
	.type        = BPF_MAP_TYPE_PERCPU_ARRAY,
	.key_size    = sizeof(__u32),
	.value_size  = sizeof(__u32),
	.max_entries = XDP_ACTION_MAX,
};

/* Keeps stats per (enum) xdp_action */
struct bpf_map_def SEC("maps") xdp_stats_map = {
	.type        = BPF_MAP_TYPE_PERCPU_ARRAY,
	.key_size    = sizeof(__u32),
	.value_size  = sizeof(struct datarec),
	.max_entries = XDP_ACTION_MAX,
};

/* Keeps stats per (enum) xdp_action */
struct bpf_map_def SEC("maps") xdp_sip_map = {
	.type        = BPF_MAP_TYPE_PERCPU_ARRAY,
	.key_size    = sizeof(__u32),
	.value_size  = sizeof(struct metrics_sip),
	.max_entries = XDP_ACTION_MAX,
};


// static __always_inline
// void xdp_get_sip_header_tcp(struct xdp_md *ctx, __u32 saddr)
// {
// 	struct metrics_sip *rec = bpf_map_lookup_elem(&xdp_sip_map, &saddr);
// 	rec->counter++;
// }

static __always_inline
__u32 xdp_stats_record_action(struct xdp_md *ctx, __u32 action, int tr_protocol)
{
	if (action >= XDP_ACTION_MAX)
		return XDP_ABORTED;

	/* Lookup in kernel BPF-side return pointer to actual data record */
	struct datarec *rec = bpf_map_lookup_elem(&xdp_stats_map, &action);
	if (!rec)
		return XDP_ABORTED;

	/* BPF_MAP_TYPE_PERCPU_ARRAY returns a data record specific to current
	 * CPU and XDP hooks runs under Softirq, which makes it safe to update
	 * without atomic operations.
	 */
	rec->rx_packets++;
	rec->rx_bytes += (ctx->data_end - ctx->data);

	switch (tr_protocol)
	{
	case (IPPROTO_ICMP):
		rec->rx_icmp++;
		break;
	case (IPPROTO_TCP):
		rec->rx_tcp++;
//		xdp_get_sip_header_tcp(ctx, saddr);
		break;
	case (IPPROTO_UDP):
		rec->rx_udp++;
		break;
	default:
	    rec->rx_other++;
		break;
	}

	return action;
}

#endif /* __XDP_STATS_KERN_H */
