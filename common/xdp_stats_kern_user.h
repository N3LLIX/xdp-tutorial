/* SPDX-License-Identifier: GPL-2.0 */

/* Used by BPF-prog kernel side BPF-progs and userspace programs,
 * for sharing xdp_stats common struct and DEFINEs.
 */
#ifndef __XDP_STATS_KERN_USER_H
#define __XDP_STATS_KERN_USER_H

struct hdr_cursor {
	void *pos;
};

struct metrics_sip {
	__u32 counter;
	__u32 src_address;
};

/* This is the data record stored in the map */
struct datarec {
	__u64 rx_packets;
	__u64 rx_bytes;
	__u64 rx_icmp;
	__u64 rx_tcp;
	__u64 rx_udp;
	__u64 rx_other;
};

#ifndef XDP_ACTION_MAX
#define XDP_ACTION_MAX (XDP_REDIRECT + 1)
#endif

#endif /* __XDP_STATS_KERN_USER_H */
